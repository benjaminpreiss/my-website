#!/bin/bash

# Execute this script to start all applications.
# Run after install.sh

source ./config-dev.sh
source ./config-prod.sh

#(cat .template-dev-env.sh | envsubst) > development.env
#(cat .template-prod-env.sh | envsubst) > production.env

#chmod 755 development.env production.env

nerdctl compose -f ./docker-compose.yml config --env-file ./.env
# nerdctl compose-f ./docker-compose.yml up --build
